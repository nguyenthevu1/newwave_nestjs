module.export = {
  name: 'default',
  type: 'postgres',
  username: 'postgres',
  password: '2602',
  database: 'ideas',
  synchronize: 'true',
  logging: 'true',
  entities: ['./src/**/*.entity.ts', './dist/**/*.entity.js'],
};
