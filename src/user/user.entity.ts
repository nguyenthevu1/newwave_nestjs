/* eslint-disable prettier/prettier */
import {
  BeforeInsert,
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm';

import * as bcrypt from 'bcryptjs';
import * as jwt from 'jsonwebtoken';
import { UserDO } from './user.dto';
// import jso

@Entity('users')
export class UserEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @CreateDateColumn()
  created: Date;

  @Column({
    type: 'text',
    unique: true,
  })
  username: string;

  @Column({
    type: 'text',
    unique: true,
  })
  numberPhone: string;

  @Column('text')
  password: string;

  @BeforeInsert()
  async hashPassword(): Promise<void> {
    this.password = await bcrypt.hash(this.password, 10);
  }

  // eslint-disable-next-line @typescript-eslint/no-inferrable-types
  toResponseObject(showToken: boolean = true): UserDO {
    const { id, created, username, numberPhone, token } = this;
    const responseObject: any = { id, created, username, numberPhone };
    if (showToken) {
      responseObject.token = token;
    }
    return responseObject;
  }

  async comparePassword(password: string) {
    return await bcrypt.compare(this.password, password);
  }

  private get token() {
    const { id, username } = this;
    return jwt.sign({ id, username }, process.env.SECRET_TOKEN, {
      expiresIn: '7d',
    });
  }
}
