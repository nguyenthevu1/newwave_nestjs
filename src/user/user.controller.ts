import { Body, Controller, Get, Post, UsePipes } from '@nestjs/common';
import { UserService } from './user.service';
import { UserDTO, User } from './user.dto';
import { ValidationPipe } from './../shared/validation.pipe';

@Controller('api/users')
export class UserController {
  constructor(private userService: UserService) {}

  @Get('')
  showAllUser() {
    return this.userService.showAllAsync();
  }

  @Get(':id')
  get(id) {
    return this.userService.showAllAsync();
  }

  @Post('login')
  @UsePipes(new ValidationPipe())
  login(@Body() data: UserDTO) {
    return this.userService.loginAsync(data);
  }

  @Post('register')
  @UsePipes(new ValidationPipe())
  register(@Body() data: User) {
    return this.userService.registerAsync(data);
  }
}
