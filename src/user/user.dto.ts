/* eslint-disable prettier/prettier */
import { IsNotEmpty } from 'class-validator';

export class UserDTO {
  @IsNotEmpty()
  numberPhone: string;

  @IsNotEmpty()
  password: string;
}

export class User {
  @IsNotEmpty()
  numberPhone: string;

  @IsNotEmpty()
  username: string;

  @IsNotEmpty()
  password: string;
}

export class UserDO {
  id: string;
  username: string;
  created: Date;
  token?: string;
}
