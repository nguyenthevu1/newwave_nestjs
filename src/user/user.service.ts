/* eslint-disable prettier/prettier */
/* eslint-disable @typescript-eslint/no-empty-function */
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserEntity } from './user.entity';
import { User, UserDO, UserDTO } from './user.dto';
import { HttpException } from '@nestjs/common';
import { HttpStatus } from '@nestjs/common';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,
  ) {}

  async showAllAsync(): Promise<UserDO[]> {
    const users = await this.userRepository.find();
    return users.map((user) => user.toResponseObject(false));
  }

  async loginAsync(data: UserDTO) {
    const { numberPhone, password } = data;

    const user = await this.userRepository.findOneBy({
      numberPhone: numberPhone,
    });

    if (!user || (await user.comparePassword(password))) {
      throw new HttpException(
        'Invalid Password or Username',
        HttpStatus.BAD_REQUEST,
      );
    }

    return user.toResponseObject();
  }

  async registerAsync(data: User): Promise<UserDO> {
    const { numberPhone } = data;
    let user = await this.userRepository.findOne({ where: { numberPhone } });

    if (user) {
      throw new HttpException('Username already exits', HttpStatus.BAD_REQUEST);
    }

    user = await this.userRepository.create(data);
    await this.userRepository.save(user);

    return user.toResponseObject(false);
  }
}
