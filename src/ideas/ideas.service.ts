/* eslint-disable @typescript-eslint/no-empty-function */
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { IdeaEntity } from './ideas.entity';
import { IdeaDTO } from './ideas.dto';

@Injectable()
export class IdeasService {
  constructor(
    @InjectRepository(IdeaEntity)
    private ideaRepository: Repository<IdeaEntity>,
  ) {}

  async showAll(): Promise<IdeaEntity[]> {
    return await this.ideaRepository.find();
  }

  async create(data: IdeaDTO): Promise<IdeaEntity> {
    const idea = await this.ideaRepository.create(data);
    await this.ideaRepository.save(idea);
    return idea;
  }

  async read(id: string): Promise<IdeaEntity> {
    const idea = await this.ideaRepository.findOneBy({ id: id });

    if (!idea) {
      throw new HttpException('Not Found', HttpStatus.NOT_FOUND);
    }
    return idea;
  }

  async update(id: string, data: Partial<IdeaDTO>): Promise<IdeaEntity> {
    let idea = await this.ideaRepository.findOneBy({ id: id });

    if (!idea) {
      throw new HttpException('Not Found', HttpStatus.NOT_FOUND);
    }

    await this.ideaRepository.update({ id }, data);
    idea = await this.ideaRepository.findOneBy({ id: id });
    return idea;
  }

  async delete(id: string): Promise<IdeaEntity> {
    const idea = await this.ideaRepository.findOneBy({ id: id });
    if (!idea) {
      throw new HttpException('Not Found', HttpStatus.NOT_FOUND);
    }

    await this.ideaRepository.delete({ id });
    return idea;
  }
}
